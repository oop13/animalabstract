/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sarocha.animalabstract;

/**
 *
 * @author Sarocha
 */
public class TestAnimal {
    public static void main(String[] args) {
        Human h1 = new Human ("Jesper");
        h1.eat();
        h1.walk();
        h1.run();
        System.out.println("h1 is animal? "+(h1 instanceof Animal));
        System.out.println("h1 is land animal? "+(h1 instanceof LandAnimal));
        Animal a1 = h1;
        System.out.println("a1 is land animal? "+(a1 instanceof LandAnimal));
        System.out.println("a1 is reptile animal? "+(a1 instanceof Reptile));
        space();
        Cat c1 = new Cat ("Chaser");
        c1.run();
        c1.sleep();
        c1.eat();
        System.out.println("c1 is animal? "+(c1 instanceof Animal));
        System.out.println("c1 is land animal? "+(c1 instanceof LandAnimal));
        Animal a2 = c1;
        System.out.println("a2 is land animal? "+(a2 instanceof LandAnimal));
        System.out.println("a2 is reptile animal? "+(a2 instanceof Reptile));
        space();
        Dog d1 = new Dog ("Binghe");
        d1.run();
        d1.walk();
        d1.eat();
        System.out.println("d1 is animal? "+(d1 instanceof Animal));
        System.out.println("d1 is land animal? "+(d1 instanceof LandAnimal));
        Animal a3 = d1;
        System.out.println("a3 is land animal? "+(a3 instanceof LandAnimal));
        System.out.println("a3 is reptile animal? "+(a3 instanceof Reptile));
        space();
        Crocodile cr1 = new Crocodile ("Cha");
        cr1.crawl();
        cr1.sleep();
        cr1.eat();
        System.out.println("cr1 is animal? "+(cr1 instanceof Animal));
        System.out.println("cr1 is reptile animal? "+(cr1 instanceof Reptile));
        Animal a4 = cr1;
        System.out.println("a4 is reptile animal? "+(a4 instanceof Reptile));
        System.out.println("a4 is land animal? "+(a4 instanceof LandAnimal));
        space();
        Snake s1 = new Snake ("Mizuki");
        s1.crawl();
        s1.sleep();
        s1.eat();
        System.out.println("s1 is animal? "+(s1 instanceof Animal));
        System.out.println("s1 is reptile animal? "+(s1 instanceof Reptile));
        Animal a5 = s1;
        System.out.println("a5 is reptile animal? "+(a5 instanceof Reptile));
        System.out.println("a5 is land animal? "+(a5 instanceof LandAnimal));
        space();
        Fish f1 = new Fish ("Garry");
        f1.swim();
        f1.eat();
        f1.sleep();
        System.out.println("f1 is animal? "+(f1 instanceof Animal));
        System.out.println("f1 is aquatic animal? "+(f1 instanceof AquaticAnimal));
        Animal a6 = f1;
        System.out.println("a6 is aquatic animal? "+(a6 instanceof AquaticAnimal));
        System.out.println("a6 is land animal? "+(a6 instanceof LandAnimal));
        space();
        Crab cb1 = new Crab ("Mr.Crab");
        cb1.swim();
        cb1.eat();
        cb1.sleep();
        System.out.println("cb1 is animal? "+(cb1 instanceof Animal));
        System.out.println("cb1 is aquatic animal? "+(cb1 instanceof AquaticAnimal));
        Animal a7 = cb1;
        System.out.println("a7 is aquatic animal? "+(a7 instanceof AquaticAnimal));
        System.out.println("a7 is land animal? "+(a7 instanceof LandAnimal));
        space();
        Bat b1 = new Bat ("Batman");
        b1.fly();
        b1.eat();
        b1.sleep();
        System.out.println("b1 is animal? "+(b1 instanceof Animal));
        System.out.println("b1 is poultry animal? "+(b1 instanceof Poultry));
        Animal a8 = b1;
        System.out.println("a8 is poultry animal? "+(a8 instanceof Poultry));
        System.out.println("a8 is land animal? "+(a8 instanceof LandAnimal));
        space();
        Bird bi1 = new Bird ("Twitty");
        bi1.fly();
        bi1.eat();
        bi1.sleep();
        System.out.println("bi1 is animal? "+(bi1 instanceof Animal));
        System.out.println("bi1 is poultry animal? "+(bi1 instanceof Poultry));
        Animal a9 = bi1;
        System.out.println("a9 is poultry animal? "+(a9 instanceof Poultry));
        System.out.println("a9 is land animal? "+(a9 instanceof LandAnimal));
        space();
        
        
    }
    public static void space() {
        System.out.println();
    }
}
